import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import 'resources/scoped_models/main_scoped_model.dart';

import './pages/authentication_page.dart';
import './pages/home_page.dart';

final MainModel _model = MainModel();

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModel<MainModel>(
        model: _model,
        child: MaterialApp(
        title: 'ORIGAMI',
        theme: ThemeData(
          primarySwatch: Colors.grey,
        ),

        routes: {
          '/': (BuildContext context) => AuthenticationPage(),
          '/home': (BuildContext context) => HomePage(),
        },
        onUnknownRoute: (settings){
          return MaterialPageRoute(builder: (context){
            return AuthenticationPage();
          });
        }
    ));
  }
}

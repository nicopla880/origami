import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:scoped_model/scoped_model.dart';

import '../model/user.dart';

import '../../utils/request.dart';

mixin UserModel on Model {

  bool userLoading = false;
  User user;

  Future<bool> login(Map<String, dynamic> map) async {


    userLoading = true;
    notifyListeners();
    await Future.delayed(Duration(seconds: 2));
    Request request = Request();

    String url = 'http://35.198.45.142:8080/oauth/token/';

    http.Response response = await request.post(url, map);

    if(request.checkResponse(response)){
      Map<String, dynamic> responseDecoded = jsonDecode(response.body);
      user = User.fromJson(responseDecoded);
      userLoading = false;
      notifyListeners();
      return true;
    }
    userLoading = false;
    notifyListeners();
    return false;
  }
}

class User{
  int id;
  String username;
  String token;
  String email;

  User.fromJson(Map<String, dynamic> json):
      username = json['username'],
      id = json['id'],
      email = json['correo'],
      token = json['token'];
}
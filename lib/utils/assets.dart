class AppAssets {

  static const String background = 'background.png';
  static const String background2x = 'background@2x.png';
  static const String origami = 'origami.png';
  static const String origami2x = 'origami@2x.png';
  static const String journey = 'agarraviaje.png';
  static const String journey2x = 'agarraviaje@2x.png';

  String imagePath(String image) {
    String path = 'assets/';
    return path + image;
  }
}

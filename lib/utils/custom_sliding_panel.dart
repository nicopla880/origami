import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'package:scoped_model/scoped_model.dart';

import '../resources/scoped_models/main_scoped_model.dart';
import '../utils/ui_elements.dart';

class CustomSlidingPanel extends StatefulWidget {
  final bool onPressed;
  final Function callback;
  CustomSlidingPanel(this.onPressed, this.callback);

  @override
  State<StatefulWidget> createState() => CustomSlidingPanelState();
}

class CustomSlidingPanelState extends State<CustomSlidingPanel>
    with WidgetsBindingObserver {
  bool status = false;
  PanelController _pc = new PanelController();

  CustomTextField _emailTextField;
  CustomTextField _passwordTextField;

  @override
  void didUpdateWidget(CustomSlidingPanel oldWidget) {
    widget.onPressed ? _pc.open() : _pc.close();
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    _emailTextField = CustomTextField().emailTextField;
    _passwordTextField = CustomTextField().passwordTextField;
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback(addPostFrameCallback);

    super.initState();
  }

  @override
  void dispose() {
    disposeAll();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return buildSlidingPanel;
  }

  Widget get buildSlidingPanel {
    return ScopedModelDescendant(
        builder: (BuildContext context, Widget child, MainModel model) {
      return SlidingUpPanel(
        isDraggable: !model.userLoading,
        parallaxEnabled: true,
        parallaxOffset: 1.0,
        controller: _pc,
        maxHeight: AppScreenSettings(context).screenHeight / 1.4,
        minHeight: AppScreenSettings(context).screenHeight / 15,
        onPanelOpened: () => widget.callback(true),
        onPanelClosed: () => widget.callback(false),
        body: Align(
          alignment: Alignment(0, 0.70),
          child: CustomText('¡Hola! ¿A dónde querés viajar?'),
        ),
        panel: _buildMainColumn,
        borderRadius: _roundedPanelBorderRadius,
        collapsed: Stack(
          children: <Widget>[
            _roundedContainer,
          ],
        ),
      );
    });
  }

  Widget get _buildMainColumn {
    return Padding(
        padding: EdgeInsets.only(
            top: AppScreenSettings(context).screenHeight / 12.5),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _buildAuthenticationButton('SIGN IN', true),
                  _buildAuthenticationButton('SIGN UP', false),
                ],
              ),
              _buildTextField(_emailTextField),
              _buildTextField(_passwordTextField),
              status ? _buildErrorText : Container(),
              _buildFlatButton,
              _buildForgotPassword
            ]));
  }

  Widget _buildTextField(CustomTextField customTextField) {
    return Container(
        width: AppScreenSettings(context).screenWidth / 1.25,
        child: TextField(
          controller: customTextField.textEditingController,
          keyboardType: customTextField.textInputType,
          obscureText: customTextField.obscure == 1 ? true : false,
          decoration: inputDecoration(customTextField),
          inputFormatters: [LengthLimitingTextInputFormatter(20)],
          style: customTextStyle(context,
              color: AppSettings.primaryColor, size: 14),
        ));
  }

  Widget _buildAuthenticationButton(String text, bool enabled) {
    return Align(
        alignment: Alignment(0, -0.80),
        child: Padding(
            padding: EdgeInsets.all(10),
            child: RaisedButton(
              shape: roundedRectangleBorder(enabled),
              color: AppSettings.primaryColor,
              disabledColor: Colors.white,
              onPressed: enabled ? () => print('AUTHWIP') : null,
              child: CustomText(
                text,
                color: enabled ? Colors.white : Colors.black45,
              ),
            )));
  }

  Widget get _buildErrorText {
    return Padding(
        padding: EdgeInsets.all(AppScreenSettings(context).padding.top),
        child: Text(
          'Nombre de usuario o contraseña incorrectos',
          style: customTextStyle(context,
              color: Colors.red,
              size: 12 * AppScreenSettings(context).textScaleFactor),
        ));
  }

  Widget get _buildFlatButton {
    double padding = AppScreenSettings(context).padding.top;

    return Padding(
        padding: EdgeInsets.only(
            left: padding, right: padding, bottom: padding, top: padding * 2),
        child: Container(
            height: AppScreenSettings(context).screenWidth / 7.5,
            width: AppScreenSettings(context).screenWidth / 1.25,
            child: FlatButton(
                shape: roundedRectangleBorder(true),
                color: AppSettings.primaryColor,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'CONTINUAR',
                      style: customTextStyle(context, color: Colors.white),
                    ),
                    SizedBox(width: 5),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.black,
                      size: 16 * AppScreenSettings(context).textScaleFactor,
                    )
                  ],
                ),
                onPressed: validate)));
  }

  Widget get _buildForgotPassword {
    return Text('Olvidé mi contraseña',
        style: customTextStyle(context,
            color: Colors.black45, size: 14, underline: true));
  }

  Widget get _roundedContainer {
    return Container(
        decoration: BoxDecoration(borderRadius: _roundedPanelBorderRadius));
  }

  InputDecoration inputDecoration(CustomTextField customTextField) {
    return InputDecoration(
        enabledBorder: underlineInputBorder(),
        focusedBorder: underlineInputBorder(),
        labelText: customTextField.label,
        labelStyle: customTextStyle(context, color: Colors.grey, size: 14),
        errorText: customTextField.validated ? null : customTextField.errorText,
        suffixIcon: customTextField.obscure == 1 || customTextField.obscure == 2
            ? IconButton(
                icon: Icon(Icons.remove_red_eye),
                color: AppSettings.primaryColor,
                onPressed: () {
                  setState(() {
                    customTextField.changeMode();
                  });
                })
            : null);
  }

  TextStyle customTextStyle(BuildContext context,
      {Color color, double size = 16, bool underline = false}) {
    return TextStyle(
        color: color,
        decoration: underline ? TextDecoration.underline : null,
        fontSize: size * AppScreenSettings(context).textScaleFactor);
  }

  UnderlineInputBorder underlineInputBorder() {
    return UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.grey, width: 1.5));
  }

  BorderRadius get _roundedPanelBorderRadius {
    return BorderRadius.only(
        topLeft: Radius.circular(45.0), topRight: Radius.circular(45.0));
  }

  RoundedRectangleBorder roundedRectangleBorder(bool enabled) {
    return RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(100.0),
        side: BorderSide(color: enabled ? Colors.transparent : Colors.black45));
  }

  void addPostFrameCallback(Duration duration) {
    _emailTextField.focusNode.addListener(() => setState(() {}));
    _passwordTextField.focusNode.addListener(() => setState(() {}));
    _emailTextField.textEditingController.addListener(() => setState(() {}));
    _passwordTextField.textEditingController.addListener(() => setState(() {}));

    setState(() {});
  }

  void disposeAll() {
    _emailTextField.focusNode.dispose();
    _passwordTextField.focusNode.dispose();
    _emailTextField.textEditingController.dispose();
    _passwordTextField.textEditingController.dispose();
  }

  void validate() async {
    MainModel model = ScopedModel.of(context);

    String emailText = _emailTextField.textEditingController.text;
    String passwordText = _passwordTextField.textEditingController.text;

    bool emailValidated = _emailTextField.validateEmail(emailText);
    bool passwordValidated = _passwordTextField.validatePassword(passwordText);

    if (emailValidated && passwordValidated) {
      Map<String, dynamic> map = {
        'username': emailText,
        'password': passwordText,
      };

      widget.callback(false);
      FocusScope.of(context).requestFocus(FocusNode());

      setState(() {});

      bool login = await model.login(map);
      if (login) {
        status = false;
        Navigator.pushReplacementNamed(context, '/home');
      } else {
        status = true;
        widget.callback(true);
      }
    }

    setState(() {});
  }
}

class CustomTextField {
  String label;
  TextEditingController textEditingController;
  TextInputType textInputType;
  FocusNode focusNode;
  int obscure;
  String errorText;
  bool validated = true;

  CustomTextField(
      {this.label,
      this.textEditingController,
      this.textInputType,
      this.focusNode,
      this.obscure,
      this.errorText,
      this.validated});

  CustomTextField get emailTextField {
    return CustomTextField(
        label: 'USUARIO',
        textEditingController: TextEditingController(),
        textInputType: TextInputType.emailAddress,
        focusNode: FocusNode(),
        obscure: 0,
        validated: true,
        errorText: 'Ingrese una nombre de usuario valido');
  }

  CustomTextField get passwordTextField {
    return CustomTextField(
      label: 'CONTRASEÑA',
      textEditingController: TextEditingController(),
      textInputType: null,
      focusNode: FocusNode(),
      obscure: 1,
      validated: true,
      errorText: 'Ingrese una contraseña valida',
    );
  }

  bool validateEmail(String value) {
    if (value.length < 2) {
      validated = false;
    } else {
      validated = true;
    }
    return validated;
  }

  bool validatePassword(String value) {
    if (value.length < 2) {
      validated = false;
    } else {
      validated = true;
    }
    return validated;
  }

  void changeMode() {
    obscure == 1 ? obscure = 2 : obscure = 1;
  }
}

import 'dart:convert';
import 'dart:io';


import 'package:http/http.dart' as http;

class Request{


  Future<http.Response> post(String url, Map<String, dynamic> map) async{
    http.Response response;

    try{
      response = await http.post(url,body: jsonEncode(map),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },)
          .timeout(const Duration(seconds: 8));
    }catch(e){
      return null;
    }


    return response;
  }

  bool checkResponse(http.Response response){
    bool validated = false;

    if(response.statusCode == 200){
      validated = true;
    }

    return validated;
  }


}
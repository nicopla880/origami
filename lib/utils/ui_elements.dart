import 'package:flutter/material.dart';

class AppSettings {

  static const primaryColor = Color(0xFF39aee4);
  static const secondaryColor = Color(0xFF00e7eb);
  static const disabledColor = Color(0xFFc6c6c6);

}


class AppScreenSettings {
  BuildContext context;
  AppScreenSettings(this.context);

  double get screenHeight {
    return MediaQuery.of(context).size.height;
  }

  double get screenWidth {
    return MediaQuery.of(context).size.width;
  }

  double get textScaleFactor {
    return MediaQuery.of(context).textScaleFactor;
  }

  EdgeInsets get padding {
    return MediaQuery.of(context).padding;
  }
}

class CustomBackground extends StatelessWidget {
  final String image;
  final Widget child;

  CustomBackground(this.image, {this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height, decoration: boxDecoration);
  }

  BoxDecoration get boxDecoration {
    return BoxDecoration(
        image: DecorationImage(
      fit: BoxFit.cover,
      image: AssetImage(image),
    ));
  }
}

class CustomText extends StatelessWidget {
  final String text;
  final Color color;

  CustomText(this.text, {this.color = Colors.white});

  @override
  Widget build(BuildContext context) {
    return Text(text, style: customTextStyle(context));
  }

  TextStyle customTextStyle(BuildContext context) {
    return TextStyle(
        color: color,
        fontSize: 16 * AppScreenSettings(context).textScaleFactor);
  }
}


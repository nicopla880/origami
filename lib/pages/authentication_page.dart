import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:scoped_model/scoped_model.dart';

import '../resources/scoped_models/main_scoped_model.dart';

import '../utils/custom_sliding_panel.dart';
import '../utils/ui_elements.dart';
import '../utils/assets.dart';

class AuthenticationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AuthenticationPageState();
  }
}

class AuthenticationPageState extends State<AuthenticationPage> {
  bool _fab = false;

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light
            .copyWith(statusBarColor: Colors.transparent),
        child: Scaffold(
            floatingActionButton: buildFab,
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            body: GestureDetector(
                onTap: () =>
                    FocusScope.of(context).requestFocus(new FocusNode()),
                child: buildMainStack)));
  }

  Widget get buildFab {
    return ScopedModelDescendant(
        builder: (BuildContext context, Widget child, MainModel model) {
      return model.userLoading
          ? Container(
              width: AppScreenSettings(context).screenWidth / 8,
              height: AppScreenSettings(context).screenWidth / 8,
              padding: EdgeInsets.all(AppScreenSettings(context).padding.top),
              margin: EdgeInsets.all(AppScreenSettings(context).padding.top),
              decoration: BoxDecoration(
                  color: AppSettings.primaryColor, shape: BoxShape.circle),
              child: SizedBox(
                  width: AppScreenSettings(context).screenWidth / 10,
                  height: AppScreenSettings(context).screenWidth / 10,
                  child: Center(
                      child: CircularProgressIndicator(
                          backgroundColor: Colors.white))))
          : AnimatedOpacity(
                  opacity: !_fab ? 1.0 : 0.0,
                  duration: Duration(milliseconds: 500),
                  child: FloatingActionButton.extended(
                      label: Row(
                        children: <Widget>[
                          CustomText('EMPEZAR'),
                          buildButtonIcon
                        ],
                      ),
                      backgroundColor: AppSettings.primaryColor,
                      onPressed: !_fab ? () {
                        setState(() {_fab = !_fab;});

                      } : null ));

    });
  }

  Widget get buildButtonIcon {
    return Padding(
        padding: EdgeInsets.only(left: 10),
        child: Icon(Icons.arrow_forward_ios,
            size: 15 * AppScreenSettings(context).textScaleFactor,
            color: AppSettings.secondaryColor));
  }

  Widget get buildMainStack {
    String background = AppAssets().imagePath(AppAssets.background);

    return Stack(children: <Widget>[
      CustomBackground(background),
      buildMainRow,
      buildSlidingPanel
    ]);
  }

  Widget get buildSlidingPanel {
    return CustomSlidingPanel(_fab, panelChanged);
  }

  Widget get buildMainRow {
    String origami = AppAssets().imagePath(AppAssets.origami);
    String journey = AppAssets().imagePath(AppAssets.journey);

    return Align(
        alignment: Alignment(0, -0.80),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 15),
                child: Image.asset(origami)),
            Image.asset(journey),
          ],
        ));
  }

  void panelChanged(bool status) {
    if (status != _fab) {
      setState(() {
        _fab = status;
      });
    }
  }
}

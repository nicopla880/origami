import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import '../resources/scoped_models/main_scoped_model.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
        builder: (BuildContext context, Widget child, MainModel model) {
      String username = model.user?.username;

      return Scaffold(
        body: Center(
            child: Text('Bienvenido $username', style: textStyle)),
      );
    });
  }

  TextStyle get textStyle {
    return TextStyle(color: Colors.black45, fontSize: 25);
  }
}
